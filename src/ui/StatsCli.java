package ui;

import util.SiteStatsWords;

import java.util.ArrayList;

public class StatsCli implements SiteStatsWords.StatsStatus {

    private SiteStatsWords mStats;

    @Override
    public void onStatsPrepared() {
        mStats.analyze();
    }

    @Override
    public void onStatsConnect() {
        System.out.println("Loading page for analysis...");
    }

    @Override
    public void onStatsParse() {
        System.out.println("Parsing content...");
    }

    @Override
    public void onStatsError(String error) {
        System.out.println(error);
    }

    @Override
    public void onStatsReady() {
        ArrayList<SiteStatsWords.WordStat> stats = mStats.getStats();
        System.out.println("The top 25 words on this page by frequency are:");
        for(int i = 0; i < 25; i++){
            System.out.println((i+1)+". "+stats.get(i).word+" ("+stats.get(i).count+")");
        }
        ArrayList<SiteStatsWords.WordStat> statsRanked = mStats.getStatsRanked();
        System.out.println("The top 25 words on this page by rank are:");
        for(int i = 0; i < 25; i++){
            System.out.println((i+1)+". "+statsRanked.get(i).word+" ("+statsRanked.get(i).count+")");
        }
    }

    private StatsCli(String arg){
        mStats = SiteStatsWords.getInstance(this);
        mStats.setUrl(arg);
    }

    public static void main(String[] args){
        if(args.length < 1){
            System.out.println("Please enter one argument; the URL that you would like analyzed.");
            System.exit(64);
        }
        if(args.length > 1){
            System.out.println("You may only enter one argument; the URL that you would like analyzed.");
            System.exit(64);
        }
        new StatsCli(args[0]);
    }

}
