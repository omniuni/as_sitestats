package ui;

import javafx.application.Application;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import util.SiteStatsWords;

import java.util.ArrayList;

public class StatsGui extends Application implements SiteStatsWords.StatsStatus {

    private static String mArgString;
    private Scene mScene;
    private Button mBtnAnalyze;
    private TextField mTfUrl;
    private SiteStatsWords mStats;
    private TableView mTable;
    private Label mLabel;
    private ProgressBar mProgress;

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/stats_gui.fxml"));
        primaryStage.setTitle("Site Stats");
        mScene = new Scene(root, 640, 400);
        primaryStage.setScene(mScene);
        primaryStage.sizeToScene();

        mBtnAnalyze = (Button) mScene.lookup("#id_button_analyze");
        mTfUrl = (TextField) mScene.lookup("#id_text_url");
        mTable = (TableView) mScene.lookup("#id_table_words");
        mLabel = (Label) mScene.lookup("#id_label_info");
        mProgress = (ProgressBar) mScene.lookup("#id_progress");

        mStats = SiteStatsWords.getInstance(this);

        setActions();

        primaryStage.show();
    }

    private void setActions(){

        if(mArgString != null) mTfUrl.setText(mArgString);

        mBtnAnalyze.setOnAction(event -> {
            String currentUrl = mTfUrl.getCharacters().toString();
            mStats.setUrl(currentUrl);
        });

    }


    public static void main(String[] args) {
        if(args.length > 0) mArgString = args[0];
        launch(args);
    }

    @Override
    public void onStatsPrepared() {
        mTable.getColumns().remove(0, mTable.getColumns().size());
        mLabel.setText("Prepared to analyze URL.");
        mProgress.setProgress(0);
        mStats.analyze();
    }

    @Override
    public void onStatsConnect() {
        mProgress.setProgress(10);
        mLabel.setText("Connecting to URL...");

    }

    @Override
    public void onStatsParse() {
        mProgress.setProgress(20);
        mLabel.setText("Parsing URL Contents...");
    }

    @Override
    public void onStatsError(String errorMessage) {
        mProgress.setProgress(0);
        mLabel.setText("Error: "+errorMessage);
    }

    @Override
    public void onStatsReady() {
        mProgress.setProgress(60);
        ArrayList<SiteStatsWords.WordStat> stats = mStats.getStats();
        populateTable(stats);
        mLabel.setText("Completed parsing URL Contents.");
        mProgress.setProgress(100);
    }

    private void populateTable(ArrayList<SiteStatsWords.WordStat> stats){

        mTable.setEditable(true);

        TableColumn<WordTableRow, String> colWord = new TableColumn<>("Word");
        colWord.setCellValueFactory(new PropertyValueFactory<>("word"));
        colWord.setMinWidth(200);

        TableColumn<WordTableRow, Integer> colCount = new TableColumn<>("Count");
        colCount.setCellValueFactory(new PropertyValueFactory<>("count"));
        colCount.setMinWidth(140);

        TableColumn<WordTableRow, Integer> colRank = new TableColumn<>("Rank");
        colRank.setCellValueFactory(new PropertyValueFactory<>("rank"));
        colRank.setMinWidth(140);

        //noinspection unchecked
        mTable.getColumns().removeAll(colWord, colCount, colRank);
        //noinspection unchecked
        mTable.getColumns().addAll(colWord, colCount, colRank);


        ObservableList<WordTableRow> data = FXCollections.observableArrayList();
        //noinspection unchecked
        mTable.setItems(data);

        for (Object stat : stats) {
            SiteStatsWords.WordStat rowObj = (SiteStatsWords.WordStat) stat;
            WordTableRow newRow = new WordTableRow(rowObj.word, rowObj.count, rowObj.rank);
            data.addAll(newRow);
        }

    }

    public static class WordTableRow{
        private final SimpleStringProperty word;
        private final SimpleIntegerProperty count;
        private final SimpleIntegerProperty rank;

        WordTableRow(String word, int count, int rank) {
            this.word = new SimpleStringProperty(word);
            this.count = new SimpleIntegerProperty(count);
            this.rank = new SimpleIntegerProperty(rank);
        }

        public String getWord() {
            return word.get();
        }

        public int getCount() {
            return count.get();
        }

        public int getRank() {
            return rank.get();
        }
    }
}
