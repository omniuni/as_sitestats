package util;

import org.jsoup.Jsoup;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Comparator;

public class SiteStatsWords {

    /**
     * Singleton Implementation
     */
    private static SiteStatsWords mInstance;
    public static SiteStatsWords getInstance(StatsStatus caller) {
        if (mInstance == null) mInstance = new SiteStatsWords(caller);
        return mInstance;
    }

    /**
     * Public Interface
     */
    public interface StatsStatus{
        void onStatsPrepared();
        void onStatsConnect();
        void onStatsParse();
        void onStatsError(String errorMessage);
        void onStatsReady();
    }

    /**
     * WordStat class
     */
    public class WordStat{
        public String word;
        public int count;
        public int rank;

        @Override
        public boolean equals(Object obj) {
            return obj.getClass() == WordStat.class && obj.equals(word);
        }

        @Override
        public String toString() {
            return "["+word+":"+count+"]";
        }
    }

    /**
     * Member (m) variables
     */
    private StatsStatus mCaller;
    private URL mUrl;
    private String mTextContent;
    private ArrayList<WordStat> mStats;

    /**
     * Constructor
     * @param caller implements interface for callbacks
     */
    private SiteStatsWords(StatsStatus caller) {
        mCaller = caller;
    }

    /**
     * Comparator used to sort statistics
     */
    private class StatSorter implements Comparator<WordStat>{
        @Override
        public int compare(WordStat o1, WordStat o2) {
            if(o1.count == o2.count) return 0;
            return (o1.count > o2.count) ? -1 : 1;
        }
    }

    /**
     * Comparator used to sort statistics by rank
     */
    private class StatSorterRank implements Comparator<WordStat>{
        @Override
        public int compare(WordStat o1, WordStat o2) {
            if(o1.rank == o2.rank) return 0;
            return (o1.rank > o2.rank) ? -1 : 1;
        }
    }

    /**
     * Set the URL to be analyzed
     * @param url to be analyzed
     * @return boolean false if there was an error
     */
    public boolean setUrl(String url){
        try {
            mUrl = new URL(url);
        } catch (MalformedURLException e) {
            mCaller.onStatsError(e.getMessage());
            return false;
        }
        if(!mUrl.getProtocol().contains("http")){
            mCaller.onStatsError("URL must be HTTP or HTTPS protocol.");
            return false;
        }
        mCaller.onStatsPrepared();
        return true;
    }

    public void analyze(){
        mStats = new ArrayList<>();
        if(getTextContent()){
            parseTextContent();
            mCaller.onStatsReady();
        }
    }

    private boolean getTextContent(){
        try {
            mTextContent = Jsoup.connect(mUrl.toString()).get().getElementsByTag("body").text();
            mCaller.onStatsConnect();
        } catch (IOException | IllegalArgumentException e) {
            mCaller.onStatsError(e.getMessage());
            return false;
        }
        if(mTextContent == null){
            mCaller.onStatsError("No content found.");
            return false;
        }
        return true;
    }

    private void parseTextContent(){

        mCaller.onStatsParse();
        mTextContent = mTextContent.replaceAll("[^a-zA-Z ']", "");
        String[] wordsArray = mTextContent.split(" ", -1);

        for (String aWordsArray : wordsArray) {

            String currentWordWithUppercase = aWordsArray.replaceAll("[^a-zA-Z]", "");
            String currentWord = currentWordWithUppercase.toLowerCase();

            if (!currentWord.trim().equals("")) {

                int rank = 2;
                if(currentWord.length() < 2) rank = rank-1;
                if(currentWord.length() < 3) rank = rank-1;
                if(currentWord.length() < 4) rank = rank-1;
                if(currentWord.length() < 5) rank = rank-1;
                if(currentWord.length() > 7) rank = rank+1;
                if(currentWord.length() > 9) rank = rank+1;
                if(!currentWordWithUppercase.equals(currentWord)) rank = rank+1;

                boolean hasBeenFound = false;
                for (int j = 0; j < mStats.size(); j++) {
                    WordStat checkWord = mStats.get(j);
                    if (checkWord.word.equals(currentWord)) {
                        checkWord.count = checkWord.count + 1;
                        checkWord.rank = checkWord.rank+rank;
                        mStats.set(j, checkWord);
                        hasBeenFound = true;
                        break;
                    }
                }
                if (!hasBeenFound) {
                    WordStat newWord = new WordStat();
                    newWord.word = currentWord;
                    newWord.count = 1;
                    mStats.add(newWord);
                }
            }

        }

    }

    public ArrayList<WordStat> getStats(){
        mStats.sort(new StatSorter());
        return mStats;
    }

    public ArrayList<WordStat> getStatsRanked(){
        mStats.sort(new StatSorterRank());
        return mStats;
    }


}
