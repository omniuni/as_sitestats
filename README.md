# SiteStats Implementation

This application, given a URL returns the top words by count on the page.

In addition to this, I have included a simple "rank" option as well. It deprioritizes words that are very short,
and increases the priotritization of words that are longer or often use capitalization. I found that this generally
gives more actually interesting results than a pure count.

The implementation itself is as a simple class with callbacks which provides an interface.

I have included two example applications that wrap it.
One is a simple command line interface. The other is a JavaFX GUI.

## Usage

The resulting .jar files can be found in the `/out/artifacts` directory, under either CLI_Jar or GUI_Jar.

Both can be used the same way:

`java -jar [jarfile] [url]`

In the case of the GUI, if you do not provide a URL, you can simply type it in once the GUI opens.

## Notes for Improvement

I wanted to note various areas that this code could be improved.

First, documentation and Unit tests are both lacking at this point. Adding both would be helpful.

Second, there are (obviously) a lot of parts of the UI that could be improved. It's my first JavaFX application.

Third is probably most important, and is the efficiency of the core class and how it counts words.

Currently, it is simply an ArrayList of Objects. I simply split the text of the page on spaces, and loop over that array.
For each item, I then loop over the ArrayList of objects and check to see if there is already one that matches the word.
This approach is relatively slow. Java has many data structures, and I believe there is one that would be much more
efficient, possibly providing a lookup instead of having to use a loop. I made the current compromise to be able
to easily sort the items to determine the "top 25".